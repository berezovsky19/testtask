# TestTask

Test project for Alpacked.
В данном файле readme буду описывать что получилось сделать, а что нет.

Опишу лучше сразу что не удалось сделать, но разобрался(если быть честным) процентов на 60-70, так как для меня это был первый опыт с подобными системами такими как Docker и Docker Hub. 
Не удалось поставить нормально виртуальную машину Чтобы смог установить docker, соответственно не удалось сделать так чтобы запускался zabbix сервер. 
И  не удалось сделать пуш через тригер GitlabCI, то-есть после описанных ниже пунктов ясно что собрать docker image и запушить на Gitlab не удалось.  
Теперь опишу то как я делал это всё или как планировал это сделать:
Шаг номер 1: установить на виртуальную машину операционную систему на ядре Linux(Ubuntu 20.04).
Шаг номер 2: надо установить дистрибутив Docker который есть в официальном репозитории Ubuntu через команду: $ sudo apt install docker-ce
после установки проверим установлен ли docker через команду: $ sudo systemctl status docker
После всех этих действий в окне консоли должен быть вывод:
docker.service - Docker Application Container Engine
     Loaded: loaded (/путь установки/; enabled; vendor preset: enabled)
     Active: active (running) since дата установки; когда было установлено
     ...

Шаг номер 3:  будет проверка к доступу к образам на Docker Hub:
Команда в консоли : $ docker run имя образа 
вывод должен быть следующим:

Output
Unable to find image 'имя образа :latest' locally
latest: Pulling from library/имя образа 
0e03bdcc26d7: Pull complete
Digest: sha256:6a65f928fb91fcfbc963f7aa6d57c8eeb426ad9a20c7ee045538ef34847f44f1
Status: Downloaded newer image for имя образа :latest

Hello from Docker!
This message shows that your installation appears to be working correctly.
...
Так как к примеру "имя образа" не было б локально размещено загрузился бы образ из docker hub`а.

Шаг номер 4: Загрузка образов Dockerfil`a в Docker Hub
Чтобы загрузить образ необходимо выполнить вход в Docker Hub через команду: $docker login -u docker-registry-username
Далее пользователю будет предложено ввести пароль для входа
Потом нужно загрузить свой образ через через команду: $docker push docker-registry-username/docker-image-name 






